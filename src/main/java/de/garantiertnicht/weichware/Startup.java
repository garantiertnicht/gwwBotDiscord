package de.garantiertnicht.weichware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.Image;
import sx.blah.discord.util.RateLimitException;
import sx.blah.discord.util.RequestBuffer;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class Startup {
    private Logger log;

    public Startup() {
        log = LoggerFactory.getLogger(this.getClass());
    }

    @EventSubscriber
    public void onReadyEvent(ReadyEvent e) {
        for(String id : Main.getCMIOptions().getOpers()) {

            if(Main.opers.contains(id)) {
                // This Oper is allredy in the opers file, so do not add it again.
                break;
            }

            IUser newOper = Main.getClient().getUserByID(id);

            if(newOper == null) {
                log.warn("The Oper {} can't be found. May this bot needs to join a server first?", id);
                break;
            }

            Main.opers.add(newOper.getID());
        }

        if(Main.opers.size() == 0) {
            log.error("There are no Opers wish could manage the bot! Exiting...");

            try {
                RequestBuffer.killAllRequests();
                e.getClient().logout();
            } catch (RateLimitException | DiscordException e1) {
                log.error("Could not gracefully logout, doing things the hard way...");
            }

            System.exit(ExitCodes.NO_OPERS);
        } else {
            log.info("{} Opers registered.", Main.opers.size());
        }

        Main.writeOpers();
        Main.loadAudio();

        if(Main.getCMIOptions().getAvatar() != null) {
            Path avatarpath = FileSystems.getDefault().getPath(Main.getCMIOptions().getAvatar());
            if(Files.exists(avatarpath)) {
                File image = new File(Main.getCMIOptions().getAvatar());
                RequestBuffer.request(() -> {
                    try {
                        e.getClient().changeAvatar(Image.forFile(image));
                    } catch(DiscordException e1) {
                        log.warn("Can't set Avatar Image.");
                    }
                });
            } else {
                log.warn("Avatar Image not {} found.", Main.getCMIOptions().getAvatar());
            }
        }
    }
}
