package de.garantiertnicht.weichware;

public abstract class ExitCodes {
    public static int OK               = 0;
    public static int UNKNOWN          = 1;
    public static int ARGUMENT_ERROR   = 2;
    public static int LOGIN_FAILED     = 3;
    public static int NO_OPERS         = 4;
    public static int NOT_GRACEFULLY   = 5;
    public static int SERVER_DIR_ERROR = 6;
}
