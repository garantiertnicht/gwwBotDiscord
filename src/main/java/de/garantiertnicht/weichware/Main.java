package de.garantiertnicht.weichware;

import com.lexicalscope.jewel.cli.ArgumentValidationException;
import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.HelpRequestedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.audio.AudioPlayer;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    private static CommandLineOptions options = null;
    private static IDiscordClient client = null;
    protected static ArrayList<String> opers = null;
    protected static HashMap<String, LinkedList<String>> whites = null;
    protected static HashMap<String, LinkedList<String>> blacks = null;
    protected static HashMap<String, LinkedList<String>> playlist = null;

    private static Logger log;

    public static void main(String... args) {
        log = LoggerFactory.getLogger(Main.class);

        opers = new ArrayList<>();
        whites = new HashMap<>();
        blacks = new HashMap<>();
        playlist = new HashMap<>();

        try {
            options = CliFactory.parseArguments(CommandLineOptions.class, args);
        } catch(HelpRequestedException e) {
            System.out.println(e.getMessage());
            System.exit(ExitCodes.OK);
        } catch(ArgumentValidationException e) {
            System.err.println(e.getMessage());
            System.exit(ExitCodes.ARGUMENT_ERROR);
        }

        Path opersFile = FileSystems.getDefault().getPath("opers");
        if(Files.exists(opersFile)) {
            File opers = new File("opers");
            try {
                Scanner scan = new Scanner(opers);
                while(scan.hasNextLine()) {
                    Main.opers.add(scan.nextLine());
                }

                scan.close();
            } catch (FileNotFoundException e) {
                log.warn("The File opers was removed in a VERY unlikly Time.");
            }
        }

        Path serversDir = FileSystems.getDefault().getPath("servers");
        if(!Files.exists(serversDir)) {
            try {
                Files.createDirectory(serversDir);
            } catch (IOException e) {
                log.warn("Can't create servers dir.");
                System.exit(ExitCodes.SERVER_DIR_ERROR);
            }
        }

        loadServerFilesToHashMap(blacks, "blackwords");
        loadServerFilesToHashMap(whites, "whitewords");

        try {
            client = getClient(options.getToken());
            client.getDispatcher().registerListener(new Startup());
            client.getDispatcher().registerListener(new Message());
            client.getDispatcher().registerListener(new Command());
        } catch (DiscordException e) {
            log.error("Could not log in, exit!");
            System.exit(ExitCodes.LOGIN_FAILED);
        }
    }

    private static IDiscordClient getClient(String token) throws DiscordException {
        return new ClientBuilder().withToken(token).login();
    }

    public static CommandLineOptions getCMIOptions() {
        return options;
    }

    public static IDiscordClient getClient() {
        return client;
    }

    public static void writeOpers() {
        String opersFile = "opers";
        Path opersPath = FileSystems.getDefault().getPath(opersFile);
        try {
            Files.write(opersPath, opers, StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }

    /**
     * Loads a HashMap from a file.
     * @param serverList The Hashmap to load in
     * @param dir The server/ dir to load from
     */
    public static void loadServerFilesToHashMap(HashMap<String, LinkedList<String>> serverList, String dir) {
        String pathString = "servers/" + dir;
        Path directory = FileSystems.getDefault().getPath(pathString);

        // Create Directory if necessary
        if(!Files.exists(directory)) {
            try {
                Files.createDirectory(directory);
            } catch (IOException e) {
                Main.log.error("Can't create servers/black dir.");
                System.exit(ExitCodes.SERVER_DIR_ERROR);
            }
        }

        // Read contents into HashMap
        try {
            File files = new File(pathString);
            ArrayList<String> keys;

            for(String filename : files.list()) {
                LinkedList<String> content = new LinkedList<>();
                File file = new File(pathString + '/' + filename);
                Scanner scanner = new Scanner(file);

                while(scanner.hasNextLine()) {
                    content.add(scanner.nextLine());
                }

                serverList.put(filename, content);
            }
        } catch (IOException e) {
            log.error("Can't access black directory contents: {}", e.getMessage());
            System.exit(ExitCodes.SERVER_DIR_ERROR);
        }
    }

    /**
     * Loads a HashMap from a file.
     * @param serverList The Hashmap to load in
     * @param dir The server/ dir to load from
     */
    public static void saveHashmapToServerFiles(HashMap<String, LinkedList<String>> serverList, String dir) throws IOException {
        String pathString = "servers/" + dir;
        Path directory = FileSystems.getDefault().getPath(pathString);

        // We assume the Directory is still there, since we created it.
        for(String server : serverList.keySet()) {
            Path filePath = FileSystems.getDefault().getPath(pathString + '/' + server);
            Files.write(filePath, serverList.get(server), StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING);
        }
    }

    public static void loadAudio() {
        loadServerFilesToHashMap(playlist, "playlists");

        for(String server : playlist.keySet()) {
            AudioPlayer player = AudioPlayer.getAudioPlayerForGuild(client.getGuildByID(server));
            player.getPlaylist().clear();
            player.setLoop(true);

            int line = 0;

            for(String element : playlist.get(server)) {
                line++;

                if(line == 1) {
                    IVoiceChannel channel = client.getVoiceChannelByID(element);
                    if(channel == null) {
                        log.warn("Voice Channel {} does not exist.", element);
                        break;
                    }

                    try {
                        channel.join();
                    } catch(MissingPermissionsException e) {
                        log.warn("No Permissions for Voice Channel {}.", element);
                        break;
                    }

                    continue;
                } else if(line == 2) {
                    try {
                        float volume = Float.valueOf(element);
                        player.setVolume(volume);
                    } catch(NumberFormatException exc) {
                        log.warn("Invalid Number format {}", element);
                        continue;
                    }

                    continue;
                }

                String filename = "music/" + element;

                if(!Files.exists(FileSystems.getDefault().getPath(filename))) {
                    log.warn("Audiofile {} not found!", filename);
                    continue;
                }

                File audiofile = new File(filename);
                try {
                    player.queue(audiofile);
                } catch(IOException e) {
                    log.warn("Error reading Audiofile {}", filename);
                } catch(UnsupportedAudioFileException e) {
                    log.warn("Audiofile {} has an unsuported Format.", filename);
                }
            }
        }
    }
}
