package de.garantiertnicht.weichware;

import com.lexicalscope.jewel.cli.Option;

import java.util.List;

public interface CommandLineOptions {
    @Option(helpRequest = true, shortName = "h", description = "Displays this Help")
    boolean getHelp();

    @Option(shortName = "t", description = "The Token of the Bot Account")
    String getToken();

    @Option(shortName = "O", description = "Opers given User IDs", defaultValue = "")
    List<String> getOpers();

    @Option(shortName = "n", description = "Sets the Name of this Bot", defaultToNull = true)
    String getName();

    @Option(shortName = "p", description = "The prefix to determinate commands", defaultValue = "!")
    char getCommandPrefix();

    @Option(shortName = "a", description = "Path to the Avatar Image", defaultToNull = true)
    String getAvatar();
}
