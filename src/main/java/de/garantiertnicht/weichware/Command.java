package de.garantiertnicht.weichware;

import de.garantiertnicht.weichware.Events.CommandEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.util.*;
import sx.blah.discord.util.audio.AudioPlayer;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.LinkedList;

public class Command {
    private ArrayList<Executable> commands;
    Logger log;

    public Command() {
        commands = new ArrayList<>();
        log = LoggerFactory.getLogger(this.getClass());

        commands.add(new Executable(Executable.PrivilegeLevel.OPER, "stop", (client, e) -> {
            sendMessage(e.getChannel(), "```Shutting down...```");

            RequestBuffer.killAllRequests();
            try {
                client.logout();
            } catch (RateLimitException | DiscordException exc) {
                log.error("Can't gracefully log out.");
                System.exit(ExitCodes.NOT_GRACEFULLY);
            }

            System.exit(ExitCodes.OK);
        }));

        commands.add(new Executable(Executable.PrivilegeLevel.OPER, "deoper", (client, e) -> {
            if(e.getArgs().length == 0) {
                sendMessage(e.getChannel(), "```You need to provide the ID of the person to deoper.```");
                return;
            }

            if(Main.opers.contains(e.getArgs()[0])) {
                Main.opers.remove(e.getArgs()[0]);
                Main.writeOpers();
                sendMessage(e.getChannel(), "```Successfully deoperd.```");

                if(Main.opers.size() == 0) {
                    log.info("The last Oper was deopered, shutting down...");

                    RequestBuffer.killAllRequests();
                    try {
                        client.logout();
                    } catch (RateLimitException | DiscordException exc) {
                        log.error("Can't gracefully log out.");
                    }

                    System.exit(ExitCodes.NO_OPERS);
                }
            } else {
                sendMessage(e.getChannel(), "```There is no Oper with this ID.```");
            }
        }));

        commands.add(new Executable(Executable.PrivilegeLevel.OPER, "oper", (client, e) -> {
            if(e.getArgs().length == 0) {
                sendMessage(e.getChannel(), "```You need to provide the ID of the person to oper.```");
                return;
            }

            if(!Main.opers.contains(e.getArgs()[0])) {
                Main.opers.add(e.getArgs()[0]);
                Main.writeOpers();
                sendMessage(e.getChannel(), "```Successfully operd.```");
            } else {
                sendMessage(e.getChannel(), "```There is allredy an Oper with this ID.```");
            }
        }));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "showfilters", (client, e) -> {
            String awnser = "```Entries of this Guild's Blacklist:";
            for(String entry : Main.blacks.get(e.getChannel().getGuild().getID())) {
                awnser += "\n * " + entry;
            }

            awnser += "\n\nEntries of this Guild's Whitelist:";

            for(String entry : Main.whites.get(e.getChannel().getGuild().getID())) {
                awnser += "\n * " + entry;
            }

            awnser += "```";
            sendMessage(e.getChannel(), awnser);
        }));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "addwhite", (client, e) -> {
            if(e.getArgs().length == 0) {
                sendMessage(e.getChannel(), "```You need to provide a word to whitelist.```");
                return;
            }
            String word = e.getArgs()[0].toLowerCase();

            if(!word.matches("\\p{Lower}*")) {
                sendMessage(e.getChannel(), "```You word may only consits out of letters.```");
                return;
            }

            String serverId = e.getChannel().getGuild().getID();

            if(Main.whites.get(serverId).contains(word)) {
                sendMessage(e.getChannel(), "```Your word is already Whitelisted.```");
                return;
            }

            for(String black : Main.blacks.get(serverId)) {
                if(word.equals(black)) {
                    sendMessage(e.getChannel(), "```Your word is already Blacklisted.```");
                    return;
                }
            }

            Main.whites.get(serverId).add(word);
            try {
                Main.saveHashmapToServerFiles(Main.whites, "whitewords");
            } catch (IOException e1) {
                sendMessage(e.getChannel(), "```Couldn't make changes permanent, please try again later.```");
                Main.whites.get(serverId).remove(word);
                return;
            }

            sendMessage(e.getChannel(), "```Successfully added a word to the Whitelist```");
        }));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "addblack", (client, e) -> {
            if(e.getArgs().length == 0) {
                sendMessage(e.getChannel(), "```You need to provide a word to whitelisted.```");
                return;
            }
            String word = e.getArgs()[0].toLowerCase();

            if(!word.matches("\\p{Lower}*")) {
                sendMessage(e.getChannel(), "```You word may only consits out of letters.```");
                return;
            }

            String serverId = e.getChannel().getGuild().getID();

            if(Main.blacks.get(serverId).contains(word)) {
                sendMessage(e.getChannel(), "```Your word is already Blacklisted.```");
                return;
            }

            for(String black : Main.whites.get(serverId)) {
                if(word.equals(black)) {
                    sendMessage(e.getChannel(), "```Your word is already Whitelisted.```");
                    return;
                }
            }

            Main.blacks.get(serverId).add(word);
            try {
                Main.saveHashmapToServerFiles(Main.blacks, "blackwords");
            } catch (IOException e1) {
                sendMessage(e.getChannel(), "```Couldn't make changes permanent, please try again later.```");
                Main.blacks.get(serverId).remove(word);
                return;
            }

            sendMessage(e.getChannel(), "```Successfully added a word to the Blacklist```");
        }));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "removewhite", (client, e) -> {
            if(e.getArgs().length == 0) {
                sendMessage(e.getChannel(), "```You need to provide a word to removed.```");
                return;
            }
            String word = e.getArgs()[0].toLowerCase();

            if(!word.matches("\\p{Lower}*")) {
                sendMessage(e.getChannel(), "```You word may only consits out of letters.```");
                return;
            }

            String serverId = e.getChannel().getGuild().getID();

            if(!Main.whites.get(serverId).contains(word)) {
                sendMessage(e.getChannel(), "```Your word is not Whitelisted.```");
                return;
            }
            Main.whites.get(serverId).remove(word);

            try {
                Main.saveHashmapToServerFiles(Main.blacks, "whitewords");
            } catch (IOException e1) {
                sendMessage(e.getChannel(), "```Couldn't make changes permanent, please try again later.```");
                Main.whites.get(serverId).add(word);
                return;
            }

            sendMessage(e.getChannel(), "```Successfully removed a word from the Whitelist```");
        }));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "removeblack", (client, e) -> {
            if(e.getArgs().length == 0) {
                sendMessage(e.getChannel(), "```You need to provide a word to removed.```");
                return;
            }
            String word = e.getArgs()[0].toLowerCase();

            if(!word.matches("\\p{Lower}*")) {
                sendMessage(e.getChannel(), "```You word may only consits out of letters.```");
                return;
            }

            String serverId = e.getChannel().getGuild().getID();

            if(!Main.blacks.get(serverId).contains(word)) {
                sendMessage(e.getChannel(), "```Your word is not Blacklisted.```");
                return;
            }
            Main.blacks.get(serverId).remove(word);

            try {
                Main.saveHashmapToServerFiles(Main.blacks, "blackwords");
            } catch (IOException e1) {
                sendMessage(e.getChannel(), "```Couldn't make changes permanent, please try again later.```");
                Main.blacks.get(serverId).add(word);
                return;
            }

            sendMessage(e.getChannel(), "```Successfully removed a word from the Blacklist```");
        }));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "clear", (client, e) -> {
            MessageList messages = e.getChannel().getMessages();

            if(e.getChannel().getMessages().size() == 0)
                return;

            RequestBuffer.request(() -> {
                try {
                    messages.deleteBefore(messages.size() - 1, messages.size() > 99 ? 99 : messages.size());
                } catch (DiscordException e1) {
                    log.warn("Cant delete the last messages in {}/{}.", e.getChannel().getGuild().getName(),
                            e.getChannel().getName());
                } catch (MissingPermissionsException e1) {
                    log.warn("Deleting Messages after clear: Permissions denied in {}/{}",
                            e.getChannel().getGuild().getName(), e.getChannel().getName());
                }
            });
        }));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "playsong", (client, e) -> {
            if(e.getArgs().length > 2) {
                sendMessage(e.getChannel(), "```You need to provide a Channel and an Audio File.");
                return;
            }

            IVoiceChannel channel = client.getVoiceChannelByID(e.getArgs()[0]);
            if(channel == null) {
                sendMessage(e.getChannel(), "```The given Channel was not found.```");
                return;
            }

            if(!Files.exists(FileSystems.getDefault().getPath("music/" + e.getArgs()[1]))) {
                sendMessage(e.getChannel(), "```Your Sound file does not exists.```");
            }

            AudioPlayer.getAudioPlayerForGuild(e.getChannel().getGuild()).setLoop(true);

            try {
                channel.join();
                AudioPlayer.getAudioPlayerForGuild(e.getChannel().getGuild()).queue(new File("music/" + e.getArgs()[1]));
            } catch (MissingPermissionsException e1) {
                e1.printStackTrace();
            } catch (UnsupportedAudioFileException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "newplaylist", (client, e) -> {
            if(e.getArgs().length < 2) {
                sendMessage(e.getChannel(), "```You need to specify a ChannelID and the Volume.```");
                return;
            }

            if(client.getVoiceChannelByID(e.getArgs()[0]) == null) {
                sendMessage(e.getChannel(), "```The specified Channel does not exists.```");
                return;
            }

            try {
                Float.parseFloat(e.getArgs()[1]);
            } catch(NumberFormatException exc) {
                sendMessage(e.getChannel(), "```Your Volume needs to be a Valid float.```");
                return;
            }

            LinkedList<String> playlist = new LinkedList<>();
            playlist.add(e.getArgs()[0]);
            playlist.add(e.getArgs()[1]);

            Main.playlist.put(e.getChannel().getGuild().getID(), playlist);
            sendMessage(e.getChannel(), "```The Playlist was cleared.```");
        }));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "addsong", ((client, e) -> {
            if(e.getArgs().length == 0) {
                sendMessage(e.getChannel(), "```You need to provide a song.```");
                return;
            }

            if(!Files.exists(FileSystems.getDefault().getPath("music/" + e.getArgs()[0]))) {
                sendMessage(e.getChannel(), "```The specified File does not exist on the server.```");
                return;
            }

            Main.playlist.get(e.getChannel().getGuild().getID()).add(e.getArgs()[0]);
            sendMessage(e.getChannel(), "```Song added.```");
        })));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "showplaylist", (client, e) -> {
            if(!Main.playlist.containsKey(e.getChannel().getGuild().getID())) {
                sendMessage(e.getChannel(), "```No playlist created.```");
            }

            String awnser = "```Entries of this Guild's Playlist:";
            for(String entry : Main.playlist.get(e.getChannel().getGuild().getID())) {
                awnser += "\n * " + entry;
            }

            awnser += "```";
            sendMessage(e.getChannel(), awnser);
        }));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "saveplaylist", ((client, e) -> {
            try {
                Main.saveHashmapToServerFiles(Main.playlist, "playlists");
            } catch (IOException e1) {
                sendMessage(e.getChannel(), "```Couldn't make changes permanent, please try again later.```");
            }

            Main.loadAudio();
            sendMessage(e.getChannel(), "```< Insert Music here :) >```");
        })));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "stopplaylist", ((client, e) -> {
            String voiceid = Main.playlist.get(e.getChannel().getGuild().getID()).get(0);
            Main.playlist.remove(e.getChannel().getGuild().getID());

            try {
                Main.saveHashmapToServerFiles(Main.playlist, "playlists");
            } catch (IOException e1) {
                sendMessage(e.getChannel(), "```Couldn't make changes permanent, please try again later.```");
                return;
            }

            IVoiceChannel channel = client.getVoiceChannelByID(voiceid);
            if(channel != null) {
                channel.leave();
            }

            Main.loadAudio();
            sendMessage(e.getChannel(), "```< Insert Sad face here :( >```");
        })));

        commands.add(new Executable(Executable.PrivilegeLevel.OP, "setvolume", ((client, e) -> {
            if(e.getArgs().length == 0) {
                sendMessage(e.getChannel(), "```You need to give the new Volume to me.```");
                return;
            }

            try {
                Float.parseFloat(e.getArgs()[0]);
            } catch(NumberFormatException exc) {
                sendMessage(e.getChannel(), "```Your Volume needs to be a Valid float.```");
                return;
            }

            Main.playlist.get(e.getChannel().getGuild().getID()).set(1, e.getArgs()[0]);

            try {
                Main.saveHashmapToServerFiles(Main.playlist, "playlists");
            } catch (IOException e1) {
                sendMessage(e.getChannel(), "```Couldn't make changes permanent, please try again later.```");
            }

            Main.loadAudio();
            sendMessage(e.getChannel(), "```Changed Volume.```");
        })));
    }

    @EventSubscriber
    public void onCommand(CommandEvent e) {
        for (Executable cmd : commands) {
           if(cmd.getCommand().equalsIgnoreCase(e.getCommand())) {
               log.info("{} issued Command {} in {}/{}",
                       e.getAuthor().getName(),
                       e.getCommand(),
                       e.getChannel().isPrivate() ? "MISSINGSTR" : e.getChannel().getGuild().getName(),
                       e.getChannel().getName());

               if(cmd.getPrivilegeLevel() == Executable.PrivilegeLevel.EVERYONE) {
                   cmd.exec(e);
               } else if(cmd.getPrivilegeLevel() == Executable.PrivilegeLevel.OP && !e.getChannel().isPrivate()) {
                   boolean found = false;

                   for(IRole role : e.getAuthor().getRolesForGuild(e.getChannel().getGuild())) {
                       if(role.getPermissions().contains(Permissions.ADMINISTRATOR)) {
                           cmd.exec(e);
                           found = true;
                           break;
                       }
                   }

                   if(!found && Main.opers.contains(e.getAuthor().getID())) {
                       cmd.exec(e);
                   }
               } else if(cmd.getPrivilegeLevel() == Executable.PrivilegeLevel.OPER &&
                       Main.opers.contains(e.getAuthor().getID())) {
                   cmd.exec(e);
               } else {
                   log.info("..insufficient Permissions.");
               }

               return;
           }
        }
    }

    public void sendMessage(IChannel channel, String message) {
            RequestBuffer.request(() -> {
                try {
                    channel.sendMessage(message);
                } catch (MissingPermissionsException exc) {
                    log.info("Could not deleteMessage Command Message in channel {}/{}: Insufficient Permissions",
                            channel.getGuild().getName(),
                            channel.getName());
                } catch (DiscordException exc) {
                    log.info("Could not deleteMessage Command Message in channel {}/{}: {}",
                            channel.getGuild().getName(),
                            channel.getName());
                    exc.getErrorMessage();
                }
            });
    }
}
