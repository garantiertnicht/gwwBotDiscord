package de.garantiertnicht.weichware;

import de.garantiertnicht.weichware.Events.CommandEvent;
import sx.blah.discord.api.IDiscordClient;

public class Executable {
    enum PrivilegeLevel {
        EVERYONE,
        OP,
        OPER
    }

    interface Command {
        void exec(IDiscordClient client, CommandEvent e);
    }

    private PrivilegeLevel privilegeLevel;
    private String command;
    private Command exec;

    public Executable(PrivilegeLevel privilegeLevel, String command, Command exec) {
        this.privilegeLevel = privilegeLevel;
        this.command = command;
        this.exec = exec;
    }

    public PrivilegeLevel getPrivilegeLevel() {
        return privilegeLevel;
    }

    public String getCommand() {
        return command;
    }

    public void exec(CommandEvent e) {
        if(!e.getChannel().isPrivate()) {
            e.deleteMessage();
        }

        exec.exec(Main.getClient(), e);
    }
}
