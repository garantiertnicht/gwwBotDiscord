package de.garantiertnicht.weichware;

import de.garantiertnicht.weichware.Events.CommandEvent;
import org.simmetrics.metrics.JaroWinkler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.MessageUpdateEvent;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

import java.util.LinkedList;

public class Message {
    Logger log;

    public Message() {
        log = LoggerFactory.getLogger(this.getClass());
    }

    @EventSubscriber
    public void onRecive(MessageReceivedEvent e) {
        IMessage msg = e.getMessage();

        // Creating Black & Whitelist for the Server if not done already.
        if(!e.getMessage().getChannel().isPrivate()) {
            if (!Main.whites.containsKey(e.getMessage().getGuild().getID())) {
                Main.whites.put(e.getMessage().getGuild().getID(), new LinkedList<>());
            }

            if (!Main.blacks.containsKey(e.getMessage().getGuild().getID())) {
                Main.blacks.put(e.getMessage().getGuild().getID(), new LinkedList<>());
            }
        }

        // If it's a command, execut it.
        if(checkIfCommand(msg)) {
            return;
        }

        // Apply Filters,
        if(!msg.getChannel().isPrivate() && !isValidMsg(msg.getChannel().getGuild().getID(), msg.getContent()) &&
                !msg.getAuthor().equals(Main.getClient().getOurUser())) {
            deleteMessage(msg);
        }
    }

    @EventSubscriber
    public void onUpdate(MessageUpdateEvent e) {
        IMessage msg = e.getNewMessage();

        // Creating Black & Whitelist for the Server if not done already.
        if(!e.getNewMessage().getChannel().isPrivate()) {
            if (!Main.whites.containsKey(e.getNewMessage().getGuild().getID())) {
                Main.whites.put(e.getNewMessage().getGuild().getID(), new LinkedList<>());
            }

            if (!Main.blacks.containsKey(e.getNewMessage().getGuild().getID())) {
                Main.blacks.put(e.getNewMessage().getGuild().getID(), new LinkedList<>());
            }
        }
        // Execute Comand if exists.
        if(checkIfCommand(msg)) {
            return;
        }

        // Apply Filters.
        if(!msg.getChannel().isPrivate() && !isValidMsg(msg.getChannel().getGuild().getID(), msg.getContent())) {
            deleteMessage(msg);
        }
    }

    public boolean checkIfCommand(IMessage msg) {
        if((msg.getChannel().isPrivate() &&
                    Main.opers.contains(msg.getAuthor().getID())
                ) ||
                msg.getContent().charAt(0) == Main.getCMIOptions().getCommandPrefix()) {

            Main.getClient().getDispatcher().dispatch(new CommandEvent(msg));
            return true;
        }

        return false;
    }

    public static boolean isValidMsg(String serverid, String msg) {
        msg = msg.toLowerCase();

        JaroWinkler alg = new JaroWinkler();

        String msg1 = "";
        for(char a : msg.toCharArray())
            if(Character.isLetter(a) || a == ' ')
                msg1 += a;

        msg = msg1;

        outerloop:
        for(String s: msg.split(" ")) {
            for (String white : Main.whites.get(serverid)) {
                if (alg.distance(s, white) < 0.05) {
                    break outerloop;
                }

                for (int startpos = 0; startpos <= s.length() - white.length(); startpos++) {
                    if (alg.distance(s.substring(startpos, startpos + white.length()), white) < 0.05) {
                        break outerloop;
                    }
                }
            }

            for (String black : Main.blacks.get(serverid)) {
                if (alg.distance(s, black) < 0.1) {
                    return false;
                }

                for (int startpos = 0; startpos <= s.length() - black.length(); startpos++) {
                    if (alg.distance(s.substring(startpos, startpos + black.length()), black) < 0.1) {
                        return false;
                    }
                }
            }
        }

        msg1 = "";
        for(char a : msg.toCharArray())
            if(Character.isLetter(a))
                msg1 += a;

        msg = msg1;

        outerloop:
        for(String s: msg.split(" ")) {
            for (String white : Main.whites.get(serverid)) {
                if (alg.distance(s, white) < 0.05) {
                    break outerloop;
                }

                for (int startpos = 0; startpos <= s.length() - white.length(); startpos++) {
                    if (alg.distance(s.substring(startpos, startpos + white.length()), white) < 0.05) {
                        break outerloop;
                    }
                }
            }

            for (String black : Main.blacks.get(serverid)) {
                if (alg.distance(s, black) < 0.1) {
                    return false;
                }

                for (int startpos = 0; startpos <= s.length() - black.length(); startpos++) {
                    if (alg.distance(s.substring(startpos, startpos + black.length()), black) < 0.1) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public void deleteMessage(IMessage msg) {
        RequestBuffer.request(() -> {
            try {
                msg.delete();
            } catch (MissingPermissionsException e) {
                log.info("Could not deleteMessage Message in channel {}/{}: Insufficient Permissions",
                        msg.getChannel().getGuild().getName(),
                        msg.getChannel().getName());
            } catch (DiscordException e) {
                log.info("Could not deleteMessage Message in channel {}/{}: {}",
                        msg.getChannel().getGuild().getName(),
                        msg.getChannel().getName());
                e.getErrorMessage();
            }
        });
    }
}
