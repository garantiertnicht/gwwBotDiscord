package de.garantiertnicht.weichware.Events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.events.Event;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

public class CommandEvent extends Event {
    private IMessage msg;
    private String command;
    private String[] args;

    private Logger log;

    public CommandEvent(IMessage msg) {
        log = LoggerFactory.getLogger(this.getClass());
        String[] splited = msg.getContent().split(" ");

        if(msg.getChannel().isPrivate()) {
            command = splited[0];
        } else {
            command = splited[0].substring(1);
        }

        args = new String[splited.length - 1];

        for(int i = 1; i < splited.length; i++) {
            args[i - 1] = splited[i];
        }

        this.msg = msg;
    }

    public String getCommand() {
        return command;
    }

    public String[] getArgs() {
        return args;
    }

    public IChannel getChannel() {
        return msg.getChannel();
    }

    public IUser getAuthor() {
        return msg.getAuthor();
    }

    public void deleteMessage() {
        RequestBuffer.request(() -> {
            try {
                msg.delete();
            } catch (MissingPermissionsException e) {
                log.info("Could not deleteMessage Command Message in channel {}/{}: Insufficient Permissions",
                        msg.getChannel().getGuild().getName(),
                        msg.getChannel().getName());
            } catch (DiscordException e) {
                log.info("Could not deleteMessage Command Message in channel {}/{}: {}",
                        msg.getChannel().getGuild().getName(),
                        msg.getChannel().getName());
                e.getErrorMessage();
            }
        });
    }
}
